var gulp = require('gulp');
var less = require('gulp-less');
var log = require('fancy-log');
var colors = require('colors/safe');

gulp.task('cssTask', function(){
    return gulp.src('css/less/*.less')
    .pipe(less().on('error', function(err){
        // don't stop watching if there is an error
        this.emit('end');
        // log the error
        log.error(colors.red.underline(err.message));
    }))
    .pipe(gulp.dest('css/'))
});

gulp.task('watch', function() {
    gulp.watch('css/less/*.less', ['cssTask'])
});